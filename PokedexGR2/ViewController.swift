//
//  ViewController.swift
//  PokedexGR2
//
//  Created by Sebastian Guerrero on 5/12/17.
//  Copyright © 2017 Sebastian Guerrero. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var imageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        //set the frame of your imageView here to automatically adopt screen size changes (e.g. by rotation or splitscreen)
        self.imageView.frame = self.view.bounds
    }


}

