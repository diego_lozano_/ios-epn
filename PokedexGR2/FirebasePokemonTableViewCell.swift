//
//  FirebasePokemonTableViewCell.swift
//  PokedexGR2
//
//  Created by Diego Lozano on 16/6/17.
//  Copyright © 2017 Sebastian Guerrero. All rights reserved.
//

import UIKit

class FirebasePokemonTableViewCell: UITableViewCell {
    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var pesoLabel: UILabel!
    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var pokemonImage: UIImageView!
    
    var pokemon:SPokemonFirebase!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fillData(){
        
        idLabel.text = "\(pokemon.id!)"
        nombreLabel.text = "\(pokemon.nombre ?? "" )"
        pesoLabel.text = "\(pokemon.peso ?? 0 ) KG"
        if pokemon.imagen == nil {
            
            let bm = SBackendManager()
            
            bm.getImage((pokemon?.id)!, completionHandler: { (imageR) in
                
                DispatchQueue.main.async {
                    self.pokemonImage.image = imageR
                }
                
            })
            
        } else {
            
            pokemonImage.image = pokemon.imagen
            
        }
        
    }

}
