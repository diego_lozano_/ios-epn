//
//  FirebasePokedexViewController.swift
//  PokedexGR2
//
//  Created by Diego Lozano on 16/6/17.
//  Copyright © 2017 Sebastian Guerrero. All rights reserved.
//

import UIKit

class FirebasePokedexViewController: UITableViewController{
    @IBOutlet var pokemonTableView: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        misPokemon.removeAll()
        let bm = SBackendManager()
        bm.getMyPokemons()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reload2), name: NSNotification.Name("reload2"), object: nil)
        
    }
    
    func reload2() {
        pokemonTableView.reloadData()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return misPokemon.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell") as! FirebasePokemonTableViewCell
        if(indexPath.row<misPokemon.count) {
            cell.pokemon = misPokemon[indexPath.row]
            cell.fillData()
        }
        return cell
        
    }


}
