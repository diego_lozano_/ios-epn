//
//  BackendManager.swift
//  PokedexGR2
//
//  Created by Sebastian Guerrero on 6/9/17.
//  Copyright © 2017 Sebastian Guerrero. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import AlamofireImage
import Firebase
import ObjectMapper

class SBackendManager{
    let database = Database.database().reference()
    
    func storePokemon(_ pokemon: SPokemon) {
        self.database.child("Pokemons").child("\(pokemon.id)").setValue(["id":pokemon.id,"nombre":pokemon.nombre,"altura":pokemon.altura,"experiencia":pokemon.experiencia,"peso":pokemon.peso,"orden":pokemon.orden])
    }
    
    func getMyPokemons() {
        misPokemon.removeAll()
        database.child("Pokemons").observe(.childAdded, with: { snapshot in
            //print(snapshot.value)
            let child = snapshot.value as! NSDictionary
            if let theJSONData = try? JSONSerialization.data(
                withJSONObject: child,
                options: []) {
                let theJSONText = String(data: theJSONData,
                                         encoding: .ascii)
                print("JSON string = \(theJSONText!)")
                let pokemonItem = Mapper<SPokemonFirebase>().map(JSONString: theJSONText!)
                //print(pokemonItem?.id)
                misPokemon.append(pokemonItem!)
                terminoSincronizar = true
            }
        })
    }


    
    
    
    
    
    func getAllPokemon () {
        
        let url = "https://pokeapi.co/api/v2/pokemon"
        
        Alamofire.request(url).responseObject { (response: DataResponse<SPokemonApiResponse>) in
            
            let pokemonResponse = response.result.value
            
            if let sPokeArray = pokemonResponse?.resultados {
                
                contador = sPokeArray.count
                
            }
        }
    }
    
    func getPokemon(_ url:String){
        
        Alamofire.request(url).responseObject { (response: DataResponse<SPokemon>) in
            
            let spokemon = response.result.value
            
            pokemonArray += [spokemon!]
            contador = contador! - 1
        }
    }
    
    func getImage(_ id:Int, completionHandler: @escaping(UIImage)->()){
        
        let url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(id).png"
        
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                completionHandler(image)
            }
        }
        
    }
}




