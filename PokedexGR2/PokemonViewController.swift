//
//  PokemonViewController.swift
//  PokedexGR2
//
//  Created by Diego Lozano on 15/6/17.
//  Copyright © 2017 Sebastian Guerrero. All rights reserved.
//

import UIKit

class PokemonViewController: UIViewController {

    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var expLabel: UILabel!
    @IBOutlet weak var pasoLabel: UILabel!
    @IBOutlet weak var alturaLabel: UILabel!
    @IBOutlet weak var ordenLabel: UILabel!
    @IBOutlet weak var imagenPokemon: UIImageView!
    
    @IBOutlet weak var guardarPokemonButton: UIButton!
    var pokemon:SPokemon!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //print(pokemon.id!)
        idLabel.text = "\(pokemon.id!)"
        nombreLabel.text = "\(pokemon.nombre ?? "" )"
        pasoLabel.text = "\(pokemon.peso ?? 0 ) pnd"
        alturaLabel.text = "\(pokemon.altura ?? 0 ) in"
        ordenLabel.text = "\(pokemon.orden!)"
        expLabel.text = "\(pokemon.experiencia ?? 0) PE"
        
        if pokemon.imagen == nil {
            
            let bm = SBackendManager()
            
            bm.getImage((pokemon?.id)!, completionHandler: { (imageR) in
                
                DispatchQueue.main.async {
                    self.imagenPokemon.image = imageR
                }
                
            })
            
        } else {
            imagenPokemon.image = pokemon.imagen
            
        }
        
        guardarPokemonButton.addTarget(self, action: #selector(self.ratingButtonTapped(_:)), for: .touchDown)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Metodo que se dispara al dar clic en el boton
    func ratingButtonTapped(_ button: UIButton) {
        print("Manda a guardar en backend")
        let bm = SBackendManager()
        bm.storePokemon(pokemon)
        alert(message: "Pokemon guardado exitosamente", title: "POKEDEX")
        //bm.getMyPokemons()
    }
    
    func alert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
