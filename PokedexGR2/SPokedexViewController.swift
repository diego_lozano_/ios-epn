//
//  SPokedexViewController.swift
//  PokedexGR2
//
//  Created by Sebastian Guerrero on 6/9/17.
//  Copyright © 2017 Sebastian Guerrero. All rights reserved.
//

import UIKit

class SPokedexViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var pokemonTableView: UITableView!
    var selectedPokemon : PokemonTableViewCell!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let bm = SBackendManager()
        bm.getAllPokemon()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: NSNotification.Name("reload"), object: nil)
        
    }
    
    func reload() {
        pokemonTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        let cell = tableView.cellForRow(at: indexPath) as! PokemonTableViewCell
        selectedPokemon = cell
        let storyBoard: UIStoryboard = UIStoryboard(name: "Pokemon", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "PokemonDetail") as! PokemonViewController
        newViewController.pokemon = selectedPokemon.pokemon!
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemonArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell") as! PokemonTableViewCell
        cell.pokemon = pokemonArray[indexPath.row]
        cell.fillData()
        return cell
    }

}
